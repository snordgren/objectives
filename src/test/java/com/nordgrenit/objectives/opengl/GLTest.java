package com.nordgrenit.objectives.opengl;

import java.nio.*;

import org.junit.*;
import org.lwjgl.*;

import com.nordgrenit.objectives.glfw.*;

public class GLTest {
	
	@Test
	public void test0() {
		GLFWContext context = new GLFWContext();
		WindowConfig config = new WindowConfig("GLTest", 800, 600, false, true, true);
		try(Window window = new Window(context, config)) {
			Shader frag = Shader.fromClasspath(ShaderType.FRAGMENT_SHADER, "0.frag");
			Shader vert = Shader.fromClasspath(ShaderType.VERTEX_SHADER, "0.vert");
			ShaderProgram program = new ShaderProgram(vert, frag);
			frag.dispose();
			vert.dispose();
			
			VertexArray array = new VertexArray().bind();
			BufferObject vbo = new BufferObject(BufferObjectType.ARRAY).bind();
			FloatBuffer vertBuf = BufferUtils.createFloatBuffer(12);
			vertBuf.put(new float[] {
					0.5f, 0.5f, 0.0f, 0.5f, -0.5f, 0.0f, -0.5f, -0.5f, 0.0f, -0.5f, 0.5f, 0.0f
			});
			vertBuf.flip();
			vbo.data(vertBuf, DataUsage.STATIC_DRAW);
			
			array.vertexPointer(0, 3, DataType.FLOAT);
			VertexArray.enableAttrib(0);
			window.setVisible(true);
			
			while(!window.isCloseRequested()) {
				context.pollEvents();
				GL.clearColor(0f, 0f, 0f, 0f);
				GL.clear(ClearTarget.COLOR_BUFFER);
				
				program.use();
				array.bind();
				array.drawArrays(0, 6);
				array.unbind();
				
				window.swapBuffers();
			}
			
			array.dispose();
			vbo.dispose();
		}
	}
}
