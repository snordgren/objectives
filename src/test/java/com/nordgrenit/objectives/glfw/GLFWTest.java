package com.nordgrenit.objectives.glfw;

import org.junit.*;

public class GLFWTest {
	@Test
	public void test() {
		GLFWContext context = new GLFWContext();
		try(Window window = new Window(context, new WindowConfig("GLFWTest", 800, 600, false, false, true))) {
			window.addKeyPressListener(event -> System.out.println(event.getKey() + " was pressed."));
			window.addKeyReleaseListener(event -> System.out.println(event.getKey() + " was released."));
			window.addKeyRepeatListener(event -> System.out.println(event.getKey() + " was repeated."));
			
			window.setVisible(true);
			while(!window.isCloseRequested()) {
				window.swapBuffers();
				context.pollEvents();
			}
		}
	}
}
