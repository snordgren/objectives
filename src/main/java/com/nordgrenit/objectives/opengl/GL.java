package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;

public class GL {
	public static void clear(ClearTarget... targets) {
		int mask = 0;
		for(ClearTarget target: targets) {
			mask |= target.getId();
		}
		glClear(mask);
		GLException.check();
	}
	
	public static void clearColor(float r, float g, float b, float a) {
		glClearColor(r, g, b, a);
	}
}
