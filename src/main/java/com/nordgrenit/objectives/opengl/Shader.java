package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL20.*;

import java.io.*;

import com.nordgrenit.objectives.*;

public class Shader implements Disposable {
	private final ShaderType type;
	private final int id;
	
	public Shader(ShaderType type, String source) {
		this.type = type;
		id = glCreateShader(type.getId());
		glShaderSource(id, source);
		glCompileShader(id);
		int compileStatus = glGetShaderi(id, GL_COMPILE_STATUS);
		int infoLogLength = glGetShaderi(id, GL_INFO_LOG_LENGTH);
		if(infoLogLength > 0) {
			throw new ShaderException(glGetShaderInfoLog(id));
		}
	}
	
	@Override
	public void dispose() {
		glDeleteShader(id);
		GLException.check();
	}
	
	public int getId() {
		return id;
	}
	
	public static Shader fromClasspath(ShaderType type, String classpath) {
		InputStream is = Shader.class.getClassLoader().getResourceAsStream(classpath);
		return fromInputStream(type, is);
	}
	
	public static Shader fromInputStream(ShaderType type, InputStream stream) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		try {
			while(stream.available() > 0) {
				output.write(stream.read());
			}
		} catch(IOException exception) {
			exception.printStackTrace();
		}
		String source = new String(output.toByteArray());
		return new Shader(type, source);
	}
}
