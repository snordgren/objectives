package com.nordgrenit.objectives.opengl;

public class SubTexture extends Texture {
	private final float u1, u2, v1, v2;
	private final Texture parent;
	
	public SubTexture(Texture parent, float u1, float v1, float u2, float v2) {
		super((int) (u2 - u1) * parent.getWidth(), (int) (v2 - v1) * parent.getHeight());
		this.parent = parent;
		this.u1 = u1;
		this.u2 = u2;
		this.v1 = v1;
		this.v2 = v2;
	}
}
