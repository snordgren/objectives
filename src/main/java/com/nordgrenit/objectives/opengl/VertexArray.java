package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL30.*;

import com.nordgrenit.objectives.*;

public class VertexArray implements Disposable {
	private final int id;
	
	public VertexArray() {
		id = glGenVertexArrays();
		GLException.check();
	}
	
	public VertexArray bind() {
		glBindVertexArray(id);
		GLException.check();
		return this;
	}
	
	@Override
	public void dispose() {
		glDeleteVertexArrays(id);
		GLException.check();
	}
	
	public void unbind() {
		glBindVertexArray(0);
	}
	
	public void vertexPointer(int index, int size, DataType type) {
		glVertexAttribPointer(index, size, type.getId(), false, 0, 0);
		GLException.check();
	}
	
	public static void disableAttrib(int index) {
		glDisableVertexAttribArray(index);
		GLException.check();
	}
	
	public static void drawArrays(int first, int count) {
		glDrawArrays(GL_TRIANGLES, first, count);
		GLException.check();
	}
	
	public static void enableAttrib(int index) {
		glEnableVertexAttribArray(index);
		GLException.check();
	}
}
