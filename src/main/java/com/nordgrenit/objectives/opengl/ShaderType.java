package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL20.*;
import static org.lwjgl.opengl.GL32.*;
import static org.lwjgl.opengl.GL40.*;
import static org.lwjgl.opengl.GL43.*;

public enum ShaderType {
	COMPUTE_SHADER(GL_COMPUTE_SHADER),
	FRAGMENT_SHADER(GL_FRAGMENT_SHADER),
	GEOMETRY_SHADER(GL_GEOMETRY_SHADER),
	TESS_CONTROL_SHADER(GL_TESS_CONTROL_SHADER),
	TESS_EVALUATION_SHADER(GL_TESS_EVALUATION_SHADER),
	VERTEX_SHADER(GL_VERTEX_SHADER);
	
	private final int id;
	
	private ShaderType(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
