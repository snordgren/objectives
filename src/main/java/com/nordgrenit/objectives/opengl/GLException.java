package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL30.*;

public class GLException extends RuntimeException {
	public GLException(String msg) {
		super(msg);
	}
	
	public static void check() {
		switch(glGetError()) {
			case GL_NO_ERROR:
				break;
			case GL_INVALID_ENUM:
				throw new GLException("GL_INVALID_ENUM");
			case GL_INVALID_VALUE:
				throw new GLException("GL_INVALID_VALUE");
			case GL_INVALID_OPERATION:
				throw new GLException("GL_INVALID_OPERATION");
			case GL_INVALID_FRAMEBUFFER_OPERATION:
				throw new GLException("GL_INVALID_FRAMEBUFFER_OPERATION");
			case GL_OUT_OF_MEMORY:
				throw new GLException("GL_OUT_OF_MEMORY");
			case GL_STACK_UNDERFLOW:
				throw new GLException("GL_STACK_UNDERFLOW");
			case GL_STACK_OVERFLOW:
				throw new GLException("GL_STACK_OVERFLOW");
		}
	}
}
