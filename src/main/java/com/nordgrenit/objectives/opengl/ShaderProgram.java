package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL20.*;

import com.nordgrenit.objectives.*;

public class ShaderProgram implements Disposable {
	private final int id;
	
	public ShaderProgram(Shader... shaders) {
		id = glCreateProgram();
		for(int i = 0; i < shaders.length; i++) {
			glAttachShader(id, shaders[i].getId());
		}
		glLinkProgram(id);
		int linkStatus = glGetProgrami(id, GL_LINK_STATUS);
		int infoLogLength = glGetProgrami(id, GL_INFO_LOG_LENGTH);
		if(infoLogLength > 0) {
			throw new ShaderException(glGetProgramInfoLog(id));
		}
		for(int i = 0; i < shaders.length; i++) {
			glDetachShader(id, shaders[i].getId());
		}
	}
	
	@Override
	public void dispose() {
		glDeleteProgram(id);
		GLException.check();
	}
	
	public void use() {
		glUseProgram(id);
		GLException.check();
	}
}
