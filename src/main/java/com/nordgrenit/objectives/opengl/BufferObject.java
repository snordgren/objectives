package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL15.*;

import java.nio.*;

import com.nordgrenit.objectives.*;

public class BufferObject implements Disposable {
	private final BufferObjectType type;
	private final int id;
	
	public BufferObject(BufferObjectType type) {
		this.type = type;
		id = glGenBuffers();
	}
	
	public BufferObject bind() {
		glBindBuffer(type.getId(), id);
		GLException.check();
		return this;
	}
	
	public void data(FloatBuffer buf, DataUsage usage) {
		glBufferData(type.getId(), buf, usage.getId());
		GLException.check();
	}
	
	public void data(IntBuffer buf, DataUsage usage) {
		glBufferData(type.getId(), buf, usage.getId());
		GLException.check();
	}
	
	@Override
	public void dispose() {
		glDeleteBuffers(id);
		GLException.check();
	}
}
