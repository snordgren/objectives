package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL12.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL14.*;

public enum TextureWrap {
	REPEAT(GL_REPEAT),
	MIRRORED_REPEAT(GL_MIRRORED_REPEAT),
	CLAMP_TO_EDGE(GL_CLAMP_TO_EDGE),
	CLAMP_TO_BORDER(GL_CLAMP_TO_BORDER);
	
	private final int id;
	
	private TextureWrap(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
