package com.nordgrenit.objectives.opengl;

public enum TextureFormat {
	;
	
	private final int id;
	
	private TextureFormat(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
	
	public static TextureFormat valueOf(int id) {
		switch(id) {
			default:
				throw new GLException("InvalidEnum");
		}
	}
}
