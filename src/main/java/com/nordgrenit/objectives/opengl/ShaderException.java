package com.nordgrenit.objectives.opengl;

public class ShaderException extends RuntimeException {
	public ShaderException(String infoLog) {
		super(infoLog);
	}
}
