package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL15.*;

public enum DataUsage {
	DYNAMIC_COPY(GL_DYNAMIC_COPY),
	DYNAMIC_DRAW(GL_DYNAMIC_DRAW),
	DYNAMIC_READ(GL_DYNAMIC_READ),
	STATIC_COPY(GL_STATIC_COPY),
	STATIC_DRAW(GL_STATIC_DRAW),
	STATIC_READ(GL_STATIC_READ),
	STREAM_COPY(GL_STREAM_COPY),
	STREAM_DRAW(GL_STREAM_DRAW),
	STREAM_READ(GL_STREAM_READ);
	
	private final int id;
	
	private DataUsage(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
