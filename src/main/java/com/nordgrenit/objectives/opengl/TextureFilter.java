package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;

public enum TextureFilter {
	LINEAR(GL_LINEAR),
	NEAREST(GL_NEAREST),
	LINEAR_MIPMAP_LINEAR(GL_LINEAR_MIPMAP_LINEAR),
	LINEAR_MIPMAP_NEAREST(GL_LINEAR_MIPMAP_NEAREST),
	NEAREST_MIPMAP_LINEAR(GL_NEAREST_MIPMAP_LINEAR),
	NEAREST_MIPMAP_NEAREST(GL_NEAREST_MIPMAP_NEAREST);
	
	private final int id;
	
	private TextureFilter(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
