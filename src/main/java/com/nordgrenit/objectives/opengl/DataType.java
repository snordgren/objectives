package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL41.*;

public enum DataType {
	BYTE(GL_BYTE),
	UNSIGNED_BYTE(GL_UNSIGNED_BYTE),
	SHORT(GL_SHORT),
	UNSIGNED_SHORT(GL_UNSIGNED_SHORT),
	FIXED(GL_FIXED),
	FLOAT(GL_FLOAT);
	
	private final int id;
	
	private DataType(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
