package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;

public enum ClearTarget {
	COLOR_BUFFER(GL_COLOR_BUFFER_BIT),
	DEPTH_BUFFER(GL_DEPTH_BUFFER_BIT),
	ACCUM_BUFFER(GL_ACCUM_BUFFER_BIT),
	STENCIL_BUFFER(GL_STENCIL_BUFFER_BIT);
	
	private final int id;
	
	private ClearTarget(int id) {
		this.id = id;
	}
	
	public int getId() {
		return id;
	}
}
