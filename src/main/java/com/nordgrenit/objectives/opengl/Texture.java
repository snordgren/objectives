package com.nordgrenit.objectives.opengl;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.stb.STBImage.*;

import java.io.*;
import java.nio.*;

import org.lwjgl.*;

public class Texture {
	private static final IntBuffer widthBuf = BufferUtils.createIntBuffer(1),
			heightBuf = BufferUtils.createIntBuffer(1), textureTypeBuf = BufferUtils.createIntBuffer(1);
	private final int width, height;
	private final int id;
	
	public Texture(ByteBuffer buf, int width, int height, TextureFormat textureType) {
		this(width, height);
		bind();
	}
	
	public Texture(int width, int height) {
		this.width = width;
		this.height = height;
		id = glGenTextures();
		GLException.check();
	}
	
	public void bind() {
		glBindTexture(GL_TEXTURE_2D, getId());
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getId() {
		return id;
	}
	
	public int getWidth() {
		return width;
	}
	
	public void image2d(ByteBuffer data, TextureFormat internalFmt, TextureFormat fmt) {
		bind();
		glTexImage2D(GL_TEXTURE_2D, 0, internalFmt.getId(), getWidth(), getHeight(), 0, fmt.getId(),
				GL_UNSIGNED_BYTE, data);
		unbind();
	}
	
	public void unbind() {
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	
	public static Texture fromByteBuffer(ByteBuffer buf) {
		ByteBuffer imageBuf = stbi_load_from_memory(buf, widthBuf, heightBuf, textureTypeBuf, 0);
		Texture texture = new Texture(imageBuf, widthBuf.get(), heightBuf.get(),
				TextureFormat.valueOf(textureTypeBuf.get()));
		stbi_image_free(imageBuf);
		return texture;
	}
	
	public static Texture fromClasspath(String classpath) {
		return null;
	}
}
