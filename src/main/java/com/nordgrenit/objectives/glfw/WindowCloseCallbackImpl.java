package com.nordgrenit.objectives.glfw;

import java.util.*;

import org.lwjgl.glfw.*;

public class WindowCloseCallbackImpl extends GLFWWindowCloseCallback {
	private final Window window;
	private final List<EventListener<WindowCloseEvent>> listeners = new ArrayList<>();
	
	public WindowCloseCallbackImpl(Window window) {
		this.window = window;
	}
	
	@Override
	public void invoke(long window) {
		final WindowCloseEvent event = new WindowCloseEvent();
		if(this.window.handle == window) {
			listeners.forEach(listener -> listener.respond(event));
		}
	}
}
