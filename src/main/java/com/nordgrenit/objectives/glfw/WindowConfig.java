package com.nordgrenit.objectives.glfw;

public class WindowConfig {
	public final int width;
	public final int height;
	public final String title;
	public final boolean fullscreen;
	public final boolean resizable;
	public final boolean vsync;
	
	public WindowConfig(String title, int width, int height, boolean fullscreen, boolean resizable,
			boolean vsync) {
		this.title = title;
		this.width = width;
		this.height = height;
		this.fullscreen = fullscreen;
		this.resizable = resizable;
		this.vsync = vsync;
	}
}
