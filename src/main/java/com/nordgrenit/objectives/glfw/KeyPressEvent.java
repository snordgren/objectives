package com.nordgrenit.objectives.glfw;

public class KeyPressEvent extends AbstractKeyEvent {
	
	public KeyPressEvent(Window window, int key, int mods) {
		super(window, key, mods);
	}
	
}
