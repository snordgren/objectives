package com.nordgrenit.objectives.glfw;

public class KeyRepeatEvent extends AbstractKeyEvent {
	
	public KeyRepeatEvent(Window window, int key, int mods) {
		super(window, key, mods);
	}
	
}
