package com.nordgrenit.objectives.glfw;

public class AbstractMouseButtonEvent extends AbstractModifierEvent {
	private final int button;
	private final Window window;
	
	public AbstractMouseButtonEvent(Window window, int button, int mods) {
		super(mods);
		this.window = window;
		this.button = button;
	}
	
	public int getButton() {
		return button;
	}
	
	public Window getWindow() {
		return window;
	}
}
