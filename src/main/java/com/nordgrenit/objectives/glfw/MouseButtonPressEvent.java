package com.nordgrenit.objectives.glfw;

public class MouseButtonPressEvent extends AbstractMouseButtonEvent {
	
	public MouseButtonPressEvent(Window window, int button, int mods) {
		super(window, button, mods);
	}
	
}
