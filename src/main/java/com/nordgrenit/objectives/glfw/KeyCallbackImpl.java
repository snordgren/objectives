package com.nordgrenit.objectives.glfw;

import static org.lwjgl.glfw.GLFW.*;

import java.util.*;

import org.lwjgl.glfw.*;

public class KeyCallbackImpl extends GLFWKeyCallback {
	private final Window window;
	final List<EventListener<KeyReleaseEvent>> releaseListeners = new ArrayList<>();
	final List<EventListener<KeyPressEvent>> pressListeners = new ArrayList<>();
	final List<EventListener<KeyRepeatEvent>> repeatListeners = new ArrayList<>();
	
	public KeyCallbackImpl(Window window) {
		this.window = window;
	}
	
	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {
		if(window == this.window.handle) {
			switch(action) {
				case GLFW_RELEASE:
					KeyReleaseEvent releaseEvent = new KeyReleaseEvent(this.window, key, mods);
					releaseListeners.forEach(listener -> listener.respond(releaseEvent));
					break;
				case GLFW_PRESS:
					KeyPressEvent pressEvent = new KeyPressEvent(this.window, key, mods);
					pressListeners.forEach(listener -> listener.respond(pressEvent));
					break;
				case GLFW_REPEAT:
					KeyRepeatEvent repeatEvent = new KeyRepeatEvent(this.window, key, mods);
					repeatListeners.forEach(listener -> listener.respond(repeatEvent));
					break;
			}
		}
	}
}
