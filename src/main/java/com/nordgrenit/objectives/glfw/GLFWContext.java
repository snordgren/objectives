package com.nordgrenit.objectives.glfw;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.glfw.GLFWErrorCallback;

import gnu.trove.map.TLongObjectMap;
import gnu.trove.map.hash.TLongObjectHashMap;

public class GLFWContext {
	final TLongObjectMap<Window> windows = new TLongObjectHashMap<>();
	
	public GLFWContext() {
		GLFWErrorCallback.createPrint(System.err).set();
		if(!glfwInit()) {
			throw new IllegalStateException("Unable to initialize GLFW.");
		}
		
	}
	
	public void dispose() {
		glfwTerminate();
	}
	
	public Window getWindow(long handle) {
		return windows.get(handle);
	}
	
	public void pollEvents() {
		glfwPollEvents();
	}
}
