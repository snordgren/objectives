package com.nordgrenit.objectives.glfw;

import static org.lwjgl.glfw.GLFW.*;

import java.util.*;

import org.lwjgl.glfw.*;

public class MouseButtonCallbackImpl extends GLFWMouseButtonCallback {
	private final Window window;
	private final List<EventListener<MouseButtonPressEvent>> pressListeners = new ArrayList<>();
	private final List<EventListener<MouseButtonReleaseEvent>> releaseListeners = new ArrayList<>();
	
	public MouseButtonCallbackImpl(Window window) {
		this.window = window;
	}
	
	@Override
	public void invoke(long window, int button, int action, int mods) {
		if(window == this.window.handle) {
			if(action == GLFW_PRESS) {
				MouseButtonPressEvent event = new MouseButtonPressEvent(this.window, button, mods);
				pressListeners.forEach(listener -> listener.respond(event));
			} else if(action == GLFW_RELEASE) {
				MouseButtonReleaseEvent event = new MouseButtonReleaseEvent(this.window, button, mods);
				releaseListeners.forEach(listener -> listener.respond(event));
			}
		}
	}
	
}
