package com.nordgrenit.objectives.glfw;

@FunctionalInterface
public interface EventListener<T> {
	public void respond(T t);
}
