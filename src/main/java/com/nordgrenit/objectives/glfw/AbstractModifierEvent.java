package com.nordgrenit.objectives.glfw;

import org.lwjgl.glfw.GLFW;

public class AbstractModifierEvent {
	private final boolean isShift, isControl, isAlt, isSuper;
	
	public AbstractModifierEvent(int mods) {
		isShift = (mods & GLFW.GLFW_MOD_SHIFT) != 0;
		isControl = (mods & GLFW.GLFW_MOD_CONTROL) != 0;
		isAlt = (mods & GLFW.GLFW_MOD_ALT) != 0;
		isSuper = (mods & GLFW.GLFW_MOD_SUPER) != 0;
		
	}
	
	public boolean isAlt() {
		return isAlt;
	}
	
	public boolean isControl() {
		return isControl;
	}
	
	public boolean isShift() {
		return isShift;
	}
	
	public boolean isSuper() {
		return isSuper;
	}
	
}
