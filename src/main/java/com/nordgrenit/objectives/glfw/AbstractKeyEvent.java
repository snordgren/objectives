package com.nordgrenit.objectives.glfw;

import org.lwjgl.glfw.GLFW;

public abstract class AbstractKeyEvent extends AbstractModifierEvent {
	private final Window window;
	
	private final int key;
	
	public AbstractKeyEvent(Window window, int key, int mods) {
		super(mods);
		this.window = window;
		this.key = key;
	}
	
	public int getKey() {
		return key;
	}
	
	public Window getWindow() {
		return window;
	}
}
