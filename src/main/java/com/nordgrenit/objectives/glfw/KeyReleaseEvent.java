package com.nordgrenit.objectives.glfw;

public class KeyReleaseEvent extends AbstractKeyEvent {
	
	public KeyReleaseEvent(Window window, int key, int mods) {
		super(window, key, mods);
	}
	
}
