package com.nordgrenit.objectives.glfw;

public class MouseButtonReleaseEvent extends AbstractMouseButtonEvent {
	
	public MouseButtonReleaseEvent(Window window, int button, int mods) {
		super(window, button, mods);
	}
	
}
