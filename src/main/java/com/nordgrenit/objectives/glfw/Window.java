package com.nordgrenit.objectives.glfw;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryUtil.*;

import org.lwjgl.glfw.*;
import org.lwjgl.glfw.GLFWImage.*;
import org.lwjgl.opengl.*;

import com.nordgrenit.objectives.*;

public class Window implements Disposable {
	private final GLFWContext context;
	final long handle;
	private final KeyCallbackImpl keyCallbackImpl;
	private final MouseButtonCallbackImpl mouseButtonCallbackImpl;
	private final WindowCloseCallbackImpl windowCloseCallbackImpl;
	private final Keyboard keyboard = new Keyboard(this);
	private boolean closeRequested = false;
	private final GLCapabilities capabilities;
	private boolean visible = false;
	
	public Window(GLFWContext context, WindowConfig configuration) {
		
		this.context = context;
		glfwDefaultWindowHints();
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, configuration.resizable ? GL_TRUE : GL_FALSE);
		long primaryMonitor = glfwGetPrimaryMonitor();
		GLFWVidMode vidMode = glfwGetVideoMode(primaryMonitor);
		
		if(configuration.fullscreen) {
			int width = vidMode.width();
			int height = vidMode.height();
			handle = glfwCreateWindow(width, height, configuration.title, primaryMonitor, NULL);
		} else {
			handle = glfwCreateWindow(configuration.width, configuration.height, configuration.title, NULL,
					NULL);
		}
		
		if(handle == NULL) {
			throw new IllegalStateException("Unable to create GLFW window.");
		}
		
		context.windows.put(handle, this);
		
		keyCallbackImpl = new KeyCallbackImpl(this);
		glfwSetKeyCallback(handle, keyCallbackImpl);
		
		mouseButtonCallbackImpl = new MouseButtonCallbackImpl(this);
		glfwSetMouseButtonCallback(handle, mouseButtonCallbackImpl);
		
		windowCloseCallbackImpl = new WindowCloseCallbackImpl(this);
		glfwSetWindowCloseCallback(handle, windowCloseCallbackImpl);
		
		if(!configuration.fullscreen) {
			glfwSetWindowPos(handle, (vidMode.width() - configuration.width) / 2,
					(vidMode.height() - configuration.height) / 2);
		}
		
		makeContextCurrent();
		if(configuration.vsync) {
			glfwSwapInterval(1);
		}
		capabilities = GL.createCapabilities();
	}
	
	public void addKeyPressListener(EventListener<KeyPressEvent> listener) {
		keyCallbackImpl.pressListeners.add(listener);
	}
	
	public void addKeyReleaseListener(EventListener<KeyReleaseEvent> listener) {
		keyCallbackImpl.releaseListeners.add(listener);
	}
	
	public void addKeyRepeatListener(EventListener<KeyRepeatEvent> listener) {
		keyCallbackImpl.repeatListeners.add(listener);
	}
	
	@Override
	public void dispose() {
		glfwDestroyWindow(handle);
	}
	
	public Keyboard getKeyboard() {
		return keyboard;
	}
	
	public boolean isCloseRequested() {
		return glfwWindowShouldClose(handle) && !closeRequested;
	}
	
	public boolean isVisible() {
		return visible;
	}
	
	public void makeContextCurrent() {
		glfwMakeContextCurrent(handle);
	}
	
	public void setCloseRequested(boolean value) {
		closeRequested = value;
	}
	
	public void setIcon(Buffer images) {
		glfwSetWindowIcon(handle, images);
	}
	
	public void setVisible(boolean value) {
		if(visible == value) {
			return;
		}
		if(value) {
			glfwShowWindow(handle);
		} else {
			glfwHideWindow(handle);
		}
		visible = value;
	}
	
	public void swapBuffers() {
		glfwSwapBuffers(handle);
	}
}
