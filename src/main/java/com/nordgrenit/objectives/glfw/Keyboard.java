package com.nordgrenit.objectives.glfw;

import static org.lwjgl.glfw.GLFW.*;

public class Keyboard {
	private final Window window;
	
	public Keyboard(Window window) {
		this.window = window;
	}
	
	public boolean isKeyDown(int key) {
		boolean isKeyDown = glfwGetKey(window.handle, key) == GLFW_PRESS;
		System.out.println(isKeyDown);
		return isKeyDown;
	}
}
