package com.nordgrenit.objectives;

import java.io.Closeable;

public interface Disposable extends Closeable {
	@Override
	public default void close() {
		dispose();
	}
	
	public void dispose();
}
